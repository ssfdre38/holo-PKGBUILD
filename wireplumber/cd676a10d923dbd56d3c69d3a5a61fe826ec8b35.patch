From cd676a10d923dbd56d3c69d3a5a61fe826ec8b35 Mon Sep 17 00:00:00 2001
From: Julian Bouzas <julian.bouzas@collabora.com>
Date: Fri, 18 Feb 2022 11:59:10 -0500
Subject: [PATCH] HACK: use filter-chain nodes if any, and hide them

This hack improves the policy to use filter-chain nodes by default if they are
available. Those nodes are then linked to the echo-cancel nodes if they are
available, otherwise the will be linked to the real audio devices.

One the other hand, similar to the previous hiding of echo-cancel nodes hack,
this patch also hides the filter chain nodes so that some applications don't get
confused when getting the real default device node.

This hack won't be needed once applications use the pipewire API to check what
real device the filter chain virtual nodes are using.
---
 modules/module-default-nodes.c               | 63 ++++++++++++++++++++
 src/config/main.lua.d/40-device-defaults.lua | 10 ++++
 src/scripts/expose-default-nodes.lua         | 20 +++++++
 src/scripts/policy-node.lua                  | 13 +++-
 4 files changed, 105 insertions(+), 1 deletion(-)

diff --git a/modules/module-default-nodes.c b/modules/module-default-nodes.c
index 80ff95ac..c5f10495 100644
--- a/modules/module-default-nodes.c
+++ b/modules/module-default-nodes.c
@@ -19,6 +19,9 @@
 #define DEFAULT_AUTO_ECHO_CANCEL TRUE
 #define DEFAULT_ECHO_CANCEL_SINK_NAME "echo-cancel-sink"
 #define DEFAULT_ECHO_CANCEL_SOURCE_NAME "echo-cancel-source"
+#define DEFAULT_AUTO_FILTER_CHAIN TRUE
+#define DEFAULT_FILTER_CHAIN_SINK_NAME "filter-chain-sink"
+#define DEFAULT_FILTER_CHAIN_SOURCE_NAME "filter-chain-source"
 
 enum {
   PROP_0,
@@ -27,6 +30,9 @@ enum {
   PROP_AUTO_ECHO_CANCEL,
   PROP_ECHO_CANCEL_SINK_NAME,
   PROP_ECHO_CANCEL_SOURCE_NAME,
+  PROP_AUTO_FILTER_CHAIN,
+  PROP_FILTER_CHAIN_SINK_NAME,
+  PROP_FILTER_CHAIN_SOURCE_NAME,
 };
 
 typedef struct _WpDefaultNode WpDefaultNode;
@@ -51,6 +57,8 @@ struct _WpDefaultNodes
   gboolean use_persistent_storage;
   gboolean auto_echo_cancel;
   gchar *echo_cancel_names[2];
+  gboolean auto_filter_chain;
+  gchar *filter_chain_names[2];
 };
 
 G_DECLARE_FINAL_TYPE (WpDefaultNodes, wp_default_nodes,
@@ -207,6 +215,21 @@ is_echo_cancel_node (WpDefaultNodes * self, WpNode *node, WpDirection direction)
   return g_strcmp0 (name, self->echo_cancel_names[direction]) == 0;
 }
 
+static gboolean
+is_filter_chain_node (WpDefaultNodes * self, WpNode *node, WpDirection direction)
+{
+  const gchar *name = wp_pipewire_object_get_property (
+      WP_PIPEWIRE_OBJECT (node), PW_KEY_NODE_NAME);
+  const gchar *virtual_str = wp_pipewire_object_get_property (
+      WP_PIPEWIRE_OBJECT (node), PW_KEY_NODE_VIRTUAL);
+  gboolean virtual = virtual_str && pw_properties_parse_bool (virtual_str);
+
+  if (!name || !virtual)
+    return FALSE;
+
+  return g_strcmp0 (name, self->filter_chain_names[direction]) == 0;
+}
+
 static WpNode *
 find_best_media_class_node (WpDefaultNodes * self, const gchar *media_class,
     const gchar *node_name, WpDirection direction, gint *priority)
@@ -237,6 +260,9 @@ find_best_media_class_node (WpDefaultNodes * self, const gchar *media_class,
       if (!node_has_available_routes (self, node))
         continue;
 
+      if (self->auto_filter_chain && is_filter_chain_node (self, node, direction))
+        prio += 30000;
+
       if (self->auto_echo_cancel && is_echo_cancel_node (self, node, direction))
         prio += 20000;
 
@@ -667,6 +693,17 @@ wp_default_nodes_set_property (GObject * object, guint property_id,
     g_clear_pointer (&self->echo_cancel_names[WP_DIRECTION_OUTPUT], g_free);
     self->echo_cancel_names[WP_DIRECTION_OUTPUT] = g_value_dup_string (value);
     break;
+  case PROP_AUTO_FILTER_CHAIN:
+    self->auto_filter_chain = g_value_get_boolean (value);
+    break;
+  case PROP_FILTER_CHAIN_SINK_NAME:
+    g_clear_pointer (&self->filter_chain_names[WP_DIRECTION_INPUT], g_free);
+    self->filter_chain_names[WP_DIRECTION_INPUT] = g_value_dup_string (value);
+    break;
+  case PROP_FILTER_CHAIN_SOURCE_NAME:
+    g_clear_pointer (&self->filter_chain_names[WP_DIRECTION_OUTPUT], g_free);
+    self->filter_chain_names[WP_DIRECTION_OUTPUT] = g_value_dup_string (value);
+    break;
   default:
     G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
     break;
@@ -720,6 +757,21 @@ wp_default_nodes_class_init (WpDefaultNodesClass * klass)
       g_param_spec_string ("echo-cancel-source-name", "echo-cancel-source-name",
           "echo-cancel-source-name", DEFAULT_ECHO_CANCEL_SOURCE_NAME,
           G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
+
+  g_object_class_install_property (object_class, PROP_AUTO_FILTER_CHAIN,
+      g_param_spec_boolean ("auto-filter-chain", "auto-filter-chain",
+          "auto-filter-chain", DEFAULT_AUTO_FILTER_CHAIN,
+          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
+
+  g_object_class_install_property (object_class, PROP_FILTER_CHAIN_SINK_NAME,
+      g_param_spec_string ("filter-chain-sink-name", "filter-chain-sink-name",
+          "filter-chain-sink-name", DEFAULT_FILTER_CHAIN_SINK_NAME,
+          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
+
+  g_object_class_install_property (object_class, PROP_FILTER_CHAIN_SOURCE_NAME,
+      g_param_spec_string ("filter-chain-source-name", "filter-chain-source-name",
+          "filter-chain-source-name", DEFAULT_FILTER_CHAIN_SOURCE_NAME,
+          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
 }
 
 WP_PLUGIN_EXPORT gboolean
@@ -730,6 +782,9 @@ wireplumber__module_init (WpCore * core, GVariant * args, GError ** error)
   gboolean auto_echo_cancel = DEFAULT_AUTO_ECHO_CANCEL;
   const gchar *echo_cancel_sink_name = DEFAULT_ECHO_CANCEL_SINK_NAME;
   const gchar *echo_cancel_source_name = DEFAULT_ECHO_CANCEL_SOURCE_NAME;
+  gboolean auto_filter_chain = DEFAULT_AUTO_FILTER_CHAIN;
+  const gchar *filter_chain_sink_name = DEFAULT_FILTER_CHAIN_SINK_NAME;
+  const gchar *filter_chain_source_name = DEFAULT_FILTER_CHAIN_SOURCE_NAME;
 
   if (args) {
     g_variant_lookup (args, "save-interval-ms", "u", &save_interval_ms);
@@ -740,6 +795,11 @@ wireplumber__module_init (WpCore * core, GVariant * args, GError ** error)
         &echo_cancel_sink_name);
     g_variant_lookup (args, "echo-cancel-source-name", "&s",
         &echo_cancel_source_name);
+    g_variant_lookup (args, "auto-filter-chain", "&s", &auto_filter_chain);
+    g_variant_lookup (args, "filter-chain-sink-name", "&s",
+        &filter_chain_sink_name);
+    g_variant_lookup (args, "filter-chain-source-name", "&s",
+        &filter_chain_source_name);
   }
 
   wp_plugin_register (g_object_new (wp_default_nodes_get_type (),
@@ -750,6 +810,9 @@ wireplumber__module_init (WpCore * core, GVariant * args, GError ** error)
           "auto-echo-cancel", auto_echo_cancel,
           "echo-cancel-sink-name", echo_cancel_sink_name,
           "echo-cancel-source-name", echo_cancel_source_name,
+          "auto-filter-chain", auto_filter_chain,
+          "filter-chain-sink-name", filter_chain_sink_name,
+          "filter-chain-source-name", filter_chain_source_name,
           NULL));
   return TRUE;
 }
diff --git a/src/config/main.lua.d/40-device-defaults.lua b/src/config/main.lua.d/40-device-defaults.lua
index 549f5658..ddb08507 100644
--- a/src/config/main.lua.d/40-device-defaults.lua
+++ b/src/config/main.lua.d/40-device-defaults.lua
@@ -17,6 +17,16 @@ device_defaults.properties = {
 
   -- Sets the default echo-cancel-source node name to automatically switch to
   ["echo-cancel-source-name"] = "echo-cancel-source",
+
+  -- Whether to auto-switch to filter chain sink and source nodes or not
+  ["auto-filter-chain"] = true,
+
+  -- Sets the default filter-chain-sink node name to automatically switch to
+  ["filter-chain-sink-name"] = "filter-chain-sink",
+
+  -- Sets the default filter-chain-source node name to automatically switch to
+  ["filter-chain-source-name"] = "filter-chain-source",
+
 }
 
 function device_defaults.enable()
diff --git a/src/scripts/expose-default-nodes.lua b/src/scripts/expose-default-nodes.lua
index 747ac6e6..d14009d4 100644
--- a/src/scripts/expose-default-nodes.lua
+++ b/src/scripts/expose-default-nodes.lua
@@ -95,6 +95,16 @@ function handleDefaultNode (si, id, name, media_class)
       local target_node = si_target:get_associated_proxy ("node")
       def_name = target_node.properties["node.name"]
       Log.info ("Echo cancel playback target " .. def_name)
+    elseif name == "filter-chain-sink" then
+      local si_target = getSiTarget ("echo-cancel-playback")
+      if si_target == nil then
+        local si_target = getSiTarget ("filter-chain-playback")
+        if si_target == nil then
+          return
+        end
+      end
+      def_name = target_node.properties["node.name"]
+      Log.info ("Filter chain playback target " .. def_name)
     end
     Log.info ("Setting default.audio.sink to " .. def_name)
     metadata:set(0, "default.audio.sink", "Spa:String:JSON",
@@ -108,6 +118,16 @@ function handleDefaultNode (si, id, name, media_class)
       local target_node = si_target:get_associated_proxy ("node")
       def_name = target_node.properties["node.name"]
       Log.info ("Echo cancel capture target " .. def_name)
+    elseif name == "filter-chain-source" then
+      local si_target = getSiTarget ("echo-cancel-capture")
+      if si_target == nil then
+        local si_target = getSiTarget ("filter-chain-capture")
+        if si_target == nil then
+          return
+        end
+      end
+      def_name = target_node.properties["node.name"]
+      Log.info ("Filter chain capture target " .. def_name)
     end
     Log.info ("Setting default.audio.source to " .. def_name)
     metadata:set(0, "default.audio.source", "Spa:String:JSON",
diff --git a/src/scripts/policy-node.lua b/src/scripts/policy-node.lua
index 15a3132b..c2c2c2bb 100644
--- a/src/scripts/policy-node.lua
+++ b/src/scripts/policy-node.lua
@@ -576,8 +576,19 @@ function handleLinkable (si)
   local exclusive = parseBool(si_props["node.exclusive"])
   local si_must_passthrough = parseBool(si_props["item.node.encoded-only"])
 
+  local si_target = nil
+
+  -- filter-chain-capture needs to be linked to echo-cancel-source if it exists
+  if si_props["node.name"] == "filter-chain-capture" then
+    si_target = linkables_om:lookup {
+        Constraint { "node.name", "=", "echo-cancel-source" }
+    }
+  end
+
   -- find defined target
-  local si_target = findDefinedTarget(si_props)
+  if si_target == nil then
+    si_target = findDefinedTarget(si_props)
+  end
   local can_passthrough = si_target and canPassthrough(si, si_target)
 
   if si_target and si_must_passthrough and not can_passthrough then
-- 
GitLab

