# <div align="center">![holo-PKGBUILD](images/steamos-logo.png)![mirror](images/mirror.png)![holo-PKGBUILD](images/steamos-logo-flipped.png)</div>

# Table of Contents

- [SteamOS 3.x (holo) PKGBUILD (Source Code) Public Mirror 🪞 (Introduction)](#steamos-3x-holo-pkgbuild-source-code-public-mirror-)
- [Shallow Clone Recommended for Submodules (Submodule Shallow Clone)](#shallow-clone-recommended-for-submodules-submodule-shallow-clone)
- [Clone Complete Repository (include Submodules)](#clone-complete-repository-include-submodules)
- [SteamOS 3.x (holo) PKGBUILD (holo-PKGBUILD) Rebase](#steamos-3x-holo-pkgbuild-holo-pkgbuild-rebase)
- 📦 [Build/Make SteamOS 3.x (holo) Package](#-buildmake-steamos-3x-holo-package)
  - 📦 [Automatically](#-automatically)
  - 📦 [Manually](#-manually)
  - ➕ [Additional Software/Packages](#-additional-softwarepackages)
- [SteamOS 3.x (holo) | Steam Deck (jupiter) PKGBUILD (Source Code) Public Mirror 🪞 Portal](#steamos-3x-holo-steam-deck-jupiter-pkgbuild-source-code-public-mirror-portal)
- 👍 [Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories](#-valve-official-steamos-3x-holo-and-steam-deck-jupiter-public-repositories-) 👍
- 🙅 [Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License](#-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license-) 👎
  - [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
  - [GPL Package Research](#gpl-package-research)
- 😉 [GitLab Repository Private-Public How-to](#-gitlab-repository-private-public-how-to-) 😂
- 💭 [Thoughts/Opinions](#-thoughtsopinions-) 💡
- 📜 [License](#-license-) 📜

# <img align="center" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="48"> SteamOS 3.x (holo) <img align="center" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="48"> PKGBUILD (Source Code) Public Mirror 🪞

<div align="center">

***[These humble public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (especially the following (elaborate/expansive) [Markdown](https://en.wikipedia.org/wiki/Markdown) documentation) are dedicated to the memory of:***

🫶 ***[Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)*** 🫶

</div>

---

Before you ask/assume (*ass-u-me*), no, this is not some [parallel universe](https://en.wikipedia.org/wiki/Multiverse) or [April Fools' Day](https://en.wikipedia.org/wiki/April_Fools'_Day) joke. [These public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) are an **unmodified 1:1 public copy/mirror** of Valve's **latest** (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)), i.e., **SteamOS 3.x (holo) source code**.

[This repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and [jupiter-PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD) have exclusively (and respectively) been modified (when necessary) to source from this public copy/mirror, i.e., [these public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) - let's call them **public mirrors**. ***To clarify/elucidate, the ONLY (necessary) modifications made are to specific PKGBUILD - pointing/redirecting to [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) instead of Valve's (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)).***

These ~~public repositories~~ (*cough*) [**public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (including [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD)) are automatically maintained via a bot; using a series of bespoke tools (I may open source these tools if there's interest). I started developing these tools on *March 3, 2022*, (after *[Steam Deck (jupiter) Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2)* ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip)) release date) while reverse engineering the (aforementioned) [Steam Deck (jupiter) Recovery Image](https://steamdeck-images.steamos.cloud/recovery/).

[These **public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) launched *April 1, 2022*, and are automatically maintained (**updated daily**) until Valve officially publicizes their (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)), i.e., **SteamOS 3.x (holo) source code**. All projects are sourced from Valve's latest [official (main) source packages](https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/).

---

<img align="left" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="48"><img align="right" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="48"><img align="left" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="48"><img align="right" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="48">

<div align="center">

***I hope the publication and maintenance of [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) provide some (much needed) transparency as well as aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers.***

</div>

---

## Shallow Clone Recommended for Submodules (Submodule Shallow Clone)

Due to the (increasingly) redundant nature of the included submodules, a **submodule shallow clone** is recommended to reduce local storage consumption and improve repository performance. A shallow clone will (locally) trim and rebase (graft) repository:

```sh
git clone --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

To shallow clone [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and submodules:

```sh
git clone --depth=1 --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

To shallow clone [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and exclude submodules (not recommended):

```sh
git clone --depth=1 https://gitlab.com/evlaV/holo-PKGBUILD.git
```

or alternatively:

```sh
git clone --depth 1 https://gitlab.com/evlaV/holo-PKGBUILD.git
```

You may consider increasing the depth option parameter/value (**1**) to extend/increase commit history (at the cost of local storage consumption). Set **d**epth **(d=number/integer)** to **30** to retain the **29 (d-1)** latest commits and graft the **30th ((d)th)** commit (variably the commit history of the past month; if repository averages one (1) commit per day). Set depth to **1** for minimum local storage consumption, i.e., the latest (head) commit (grafted); no commit history (recommended).

To (re)shallow existing repository or retain shallow clone depth during/after Git fetch/pull (merge) operations, use option(s) `--depth=n` or `--depth=n --rebase` respectively:

```sh
git fetch --depth=1
git rebase
```

or alternatively:

```sh
git pull --depth=1 --rebase
```

- Optionally prune (potential) loose objects (trim) after (re)shallow:

   ```sh
   git gc --prune=now
   ```

---

To unshallow repository, i.e., restore complete/full clone depth (be prepared for a large download):

```sh
git fetch --unshallow
```

or alternatively:

```sh
git pull --unshallow
```

## Clone Complete Repository (include Submodules)

Use option `--recurse-submodules` during Git clone to include submodules:

```sh
git clone --recurse-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/holo-PKGBUILD.git
git submodule update --init --recursive
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/holo-PKGBUILD.git
git submodule init --recursive
git submodule update --recursive
```

---

Use option `--recurse-submodules` during Git fetch/pull (merge) operations to include submodules:

```sh
git fetch --recurse-submodules
```

or alternatively:

```sh
git pull --recurse-submodules
```

## SteamOS 3.x (holo) PKGBUILD (holo-PKGBUILD) Rebase

[This repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) may occasionally be rebased. This will (destructively) remove obsolete commit history and data (to reduce repository size) and improve repository performance, but may disrupt normal Git fetch/pull (merge) operations. If you receive a branch diverged error message upon Git fetch/pull (merge):

```sh
git fetch
git rebase
```

or alternatively:

```sh
git pull --rebase
```

**This will result in (local) commit history and data loss.** *This commit history and data loss is composed/consisting completely of obsolete data which has since been depreciated or replaced by (a) newer/later revision(s).*

> **Latest Rebase: January 11, 2023**

## 📦 Build/Make SteamOS 3.x (holo) Package

### 📦 Automatically

```sh
  usage: ./build.sh [package_dirname ...] [option ...] [makepkg_option ...]
example: ./build.sh steamos-efi linux holo-keyring --srcinfo -f

options:
  --srcinfo         enable (AUR) .SRCINFO generation (before make package)
  --srcinfo-only    ONLY generate (AUR) .SRCINFO (disable/skip make package)

make all (found) package(s) if package (directory name (dirname)) not specified

see makepkg help ('makepkg -h', 'makepkg --help') for makepkg options
```

Run ([included](build.sh)) `./build.sh` to automatically make all (found) or specified pacman packages (PKGBUILD); optionally specify package (directory name (dirname)) as argument(s) to make instead of all (found). Use option `--srcinfo` to enable (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) generation (before make package) or `--srcinfo-only` to ONLY generate (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) (disable/skip make package).

Run ([included](build.sh)) `./build.sh -h` for usage instructions.

### 📦 Manually

Change directory to respective pacman package(s) (PKGBUILD) e.g., `cd steamos-efi`

Make respective pacman package (PKGBUILD):

```sh
makepkg
```

Install (potentially) missing dependencies (with pacman) then make respective pacman package (PKGBUILD):

```sh
makepkg -s
```

Install (potentially) missing dependencies (with pacman) then make and install respective pacman package (PKGBUILD):

```sh
makepkg -is
```

Install (potentially) missing dependencies (with pacman) then clean, make, and install respective pacman package (PKGBUILD):

```sh
makepkg -Cis
```

Run `makepkg -h` or see: [makepkg usage](https://wiki.archlinux.org/title/Makepkg#Usage) for detailed usage instructions.

---

### ➕ Additional Software/Packages

- [build.sh](build.sh) | [documentation](#-automatically) | [PKGBUILD](map/PKGBUILD) - make all (found) or specified pacman packages (PKGBUILD)

Steam Deck (jupiter) | jupiter-PKGBUILD:

- [jupiter-bios-tool.py](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-tool.py) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-tool/PKGBUILD) - analyze, verify, backup/generate/inject UID, and dynamically trim ✂️ any/all Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd)
- **jupiter-bios-unlock**
  - [jupiter-bios-unlock.c](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock.c) | [x86_64 prebuilt C binary](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/bin/jupiter-bios-unlock) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-c-binary-jupiter-bios-unlockc-preferredprimary-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock/PKGBUILD) | [PKGBUILD (bin)](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock-bin/PKGBUILD) - enhanced/extended edition (e.g., unlock+lock support) of [SD_Unlocker.c](https://gist.github.com/SmokelessCPUv2/8c1e6559031e199d9a678c9fe2ebf7d4) (preferred/primary)
  - [jupiter-bios-unlock.py](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock.py) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-python-jupiter-bios-unlockpy-alternativesecondary-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock-python/PKGBUILD) - Python (wrapper) equivalent (alternative/secondary)

---

## <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="32"></a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD">SteamOS 3.x (holo)</a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="32"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-3.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-4.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="32"></a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD">Steam Deck (jupiter)</a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="32"></a> PKGBUILD (Source Code) Public Mirror 🪞 Portal <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-5.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-6.png" height="32" width="4"></a>

<div align="center"><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="holo-PKGBUILD" src="images/steamos-logo.png" width="128"><img alt="mirror" src="images/mirror.png" height="128"><img alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="128"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="portal" src="images/portal-1.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="portal" src="images/portal-2.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="128"><img alt="mirror" src="images/mirror.png" height="128"><img alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="128"></a></div>

---

## 👍 Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories 👍

(Pitiful) Table of Valve's officially public SteamOS 3.x (holo) and Steam Deck (jupiter) repositories:

| Repository | Branch | Release Date |
| :--- | :---: | ---: |
| [wireplumber](https://gitlab.steamos.cloud/jupiter/wireplumber) | jupiter | June 30, 2022 |
| [steamos-devkit-service](https://gitlab.steamos.cloud/devkit/steamos-devkit-service) | holo* / jupiter | March 30, 2022 |
| [steamos-devkit-client](https://gitlab.steamos.cloud/devkit/steamos-devkit) | - | March 9, 2022 |
| [steam-im-modules](https://github.com/valve-project/steam-qt-keyboard-plugin) | holo* / jupiter | February 23, 2022 |
| [gamescope](https://github.com/ValveSoftware/gamescope) ([@Plagman](https://github.com/Plagman/gamescope)) | holo* / jupiter | predates SteamOS 3.x / Steam Deck |

***Table contains ONLY software developed (or modified) by Valve, i.e., excludes unmodified public software.***

> *Denotes no respective PKGBUILD currently exists (i.e., no dedicated build/package).

---

## 🙅 Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License 👎

> [There are hundreds of missing GPL packages' source code](#gpl-package-research) - notable examples: `pacman` and `pacman-mirrorlist` - **I believe the latter may have been intentionally omitted to abscond Valve's source package location ([smoking gun](https://en.wikipedia.org/wiki/Smoking_gun) perhaps?)**

[GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are those who **distribute products based on GPL software without complying with the license conditions**; such as **providing the *complete* source code**, a copy of the license, and **a (written) offer for source code**. **GPL violations are considered a form of infringing use of the software and can be legally pursued by the copyright holders.**

These [GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are unfair and disrespectful to the free software community and the authors of the respective software. They are taking advantage of the benefits of GPL software without honoring the obligations and the spirit of the license. They deprive users of their freedom to study, modify, and share the software. They should be educated about the meaning and importance of GPL and other free licenses, and be encouraged to adopt ethical/sound practices for the incorporation of GPL components into their products. They should also be held accountable for their actions and be required to remedy their violations by complying with the license terms or ceasing distribution of the infringing products. They should additionally be deterred from continuing and/or repeating violations via public awareness campaigns and legal actions. **I'm endeavoring on both fronts (public awareness and legal action) to right (and *write*) these wrongs.**

[**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) should respect the GPL and comply with its terms as soon as possible (it's been over **18 months** now). They should also communicate clearly and transparently about their use of GPL software and their "plans" to adequately release the source code. They should also collaborate with the free software community and contribute to the development and improvement of GPL software. A good/healthy start would be (finally) opening dialogue with me. In case you hadn't noticed, I'm quite proficient and have been **doing your job** for over **18 months** now. It's about time we had a **real discussion**. I would greatly appreciate if someone from [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and/or [**Igalia**](https://www.igalia.com/about/) would (finally) respond to me and address these grievances.

A very bad trend has been set and the status quo (creatively) continued regarding 🄯 [copyleft](https://en.wikipedia.org/wiki/Copyleft). Violations are frequent, and violators are infrequently held accountable. I begrudgingly feel copyleft is seen as an [honor system](https://en.wikipedia.org/wiki/Honor_system), and you should (realistically) presume all your copyleft projects and contributions will be interpreted/used with a [permissive license](https://en.wikipedia.org/wiki/Permissive_software_license). So you might as well pick a "good/decent" permissive (or [middle ground](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)) license (since that's unfortunately the (capitalistic/corporate) world we live in) or [release into the public domain](https://unlicense.org/). I've personally demonstrated both with this project ([LICENSE](LICENSE), [UNLICENSE](UNLICENSE), and [0BSD](LICENSE.0BSD)).

I've privately informed (and now publicly informed and condemned) both [**Collabora**](https://www.collabora.com/about-us/) and [**Igalia**](https://www.igalia.com/about/) of the following (neither have responded):

`"There's a conflict of interest working and/or consulting with Valve on this particular project (SteamOS 3.x / Steam Deck)."`

**(March 25, 2023):** The [FSF](https://www.fsf.org/about/) (`rms`) has demonstrated the following to me (in the case of Valve) regarding [defending their GPL](https://www.gnu.org/licenses/):

[rms](https://en.wikipedia.org/wiki/Richard_Stallman):

`"The FSF is short-handed in the legal area. Given how little it could hope to achieve, I tend to think that this would not be an efficient choice of priorities (though I'm not the one who decides nowadays). I don't think I could get a staff person to verify the origin of the copied code in less than months."`

This interaction has (sadly) not instilled much faith in the FSF (or the GPL) and only further solidifies my license recommendations above. Regardless, thank you `rms`, for your time, honesty/transparency, inquiries/interest, and recommendation. You've been the most communicative and inquisitive person I've interacted with regarding this. I'm sorry our discussions were centered around this deplorable injustice.

I'm presently moving forward with the [Software Freedom Conservancy](https://sfconservancy.org/about/) (with `rms`'s recommendation/referral). **PROGRESS PENDING**

**Come along with me as I foolishly try and take the pulse of a piece of the corporate software/technology world in 2023 ... It will be fun, they said.**

I'm well aware that many people don't technically fully comprehend/understand the GPL. This is a far bigger (systemic) issue than simply Valve, Collabora, and Igalia. I'd argue most people are ignorant and/or misinformed regarding the GPL. I **highly recommend** ALL Linux/OSS users/enthusiasts, especially if you work for a company which may utilize (and also potentially violate) GPL software, **THOROUGHLY READ THE GPL**. The following is pretty well documented (shocker) in the GPL (if only people could be bothered enough to actually read it).

The GPL (GPL2, GPL3, AGPL, and LGPL3) stipulates that sources must accompany binaries, otherwise a (written) offer for source code (or equivalent stipulated offer) is required. See GPL2 Section 3, or GPL3 Section 6 for accepted methods of distributing sources. If one or more of these methods is not followed by Valve, then a GPL violation has occurred. I presently count [~~dozens~~](#third-party-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license) [hundreds](#gpl-package-research) of GPL violations which have been unresolved for over **18 months** now.

See: [GPL FAQ](https://www.gnu.org/licenses/gpl-faq.html#WhatDoesWrittenOfferValid)

*"If you choose to provide source through a written offer, then anybody who requests the source from you is entitled to receive it."*

*"If you commercially distribute binaries not accompanied with source code, the GPL says you must provide a written offer to distribute the source code later."*

Thank you for taking the time to learn a bit about proper copyleft (GPL) software distribution. I hope you've learned not just to comprehend/understand the GPL, but to also value and respect the free/libre license. Could the community do us all a favor and help inform Valve, Collabora, and Igalia of this fact?

But don't take my word for it, the following is a excerpt from my aforementioned discussion with `rms`:

`"Selling the computer is distribution of the preinstalled binaries of GPL-covered programs. GPL 2 and GPL 3 (and AGPL also, and LGPL 3) all require making the source available along with the binaries.`

`[Section] 6 of GPL 3 states the accepted ways of making the source available.`

`If Valve does not do any of those ways, it is violating the GPL."`

---

> **SteamOS 3.x (holo) / Steam Deck (jupiter) Release Date: [February 25, 2022](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)**
>
> [**Number of Days Valve, Collabora, and Igalia are in Violation of GNU License (i.e., since Steam Deck release)**](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)

**"If it looks like a duck, swims like a duck, and quacks like a duck, then it probably is a duck."**

<img alt="the cake is a lie." src="images/the-cake-is-a-lie.jpg" width="512">

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

***But not this cake! Happy Cake Day Steam Deck! Thank you Valve for showing the world GNU/Linux is the true home of PC gaming! Please support [free software](https://www.gnu.org/software/). (February 25, 2023)***

<img alt="the cake is not a lie." src="images/the-cake-is-not-a-lie.jpg" width="512">

---

### Third Party Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License

Table of packages/repositories **Valve, Collabora, and Igalia are in Violation of respective GNU License(s):**

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring) | holo, jupiter | [GPLv3](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [bluez](https://git.kernel.org/pub/scm/bluetooth/bluez.git) | holo, jupiter | [GPLv2](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/COPYING) | **obfuscated** source code* |
| [bmap-tools](https://github.com/intel/bmap-tools) | holo | [GPLv2](https://github.com/intel/bmap-tools/blob/master/LICENSE) | **obfuscated** source code* |
| [calamares](https://github.com/calamares/calamares) | holo | [GPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/GPL-3.0-or-later.txt), [LGPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-2.1-only.txt) | **obfuscated** source code* |
| [casync](https://github.com/systemd/casync) | holo | [LGPLv2.1](https://github.com/systemd/casync/blob/main/LICENSE.LGPL2.1) | **obfuscated** source code* |
| [ckbcomp](https://salsa.debian.org/installer-team/console-setup) | holo | [GPLv2](https://salsa.debian.org/installer-team/console-setup/-/blob/master/GPL-2) | **obfuscated** source code* |
| [dkms](https://github.com/dell/dkms) | holo | [GPLv2](https://github.com/dell/dkms/blob/master/COPYING) | **obfuscated** source code* |
| [f3](https://github.com/AltraMayor/f3) | jupiter | [GPLv3](https://github.com/AltraMayor/f3/blob/master/LICENSE) | **obfuscated** source code* |
| [ffmpeg](https://git.ffmpeg.org/gitweb/ffmpeg) | holo | [GPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv3), [GPLv2](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv2), [LGPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv3), [LGPLv2.1](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv2.1) | **obfuscated** source code* |
| [flatpak](https://github.com/flatpak/flatpak) | jupiter | [LGPLv2.1](https://github.com/flatpak/flatpak/blob/main/COPYING) | **obfuscated** source code* |
| [grub](https://www.gnu.org/software/grub/) | holo, jupiter | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) | **obfuscated** source code* |
| [holo-keyring](https://gitlab.com/evlaV/holo-keyring) (fork of [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring)) | holo | [GPLv3](https://gitlab.com/evlaV/holo-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [ibus-anthy](https://github.com/ibus/ibus-anthy) | jupiter | [GPLv2](https://github.com/ibus/ibus-anthy/blob/main/COPYING) | **obfuscated** source code* |
| [ibus-table-cangjie-lite](https://github.com/mike-fabian/ibus-table-chinese) | jupiter | [GPLv3](https://github.com/mike-fabian/ibus-table-chinese/blob/main/COPYING) | **obfuscated** source code* |
| [iwd](https://git.kernel.org/pub/scm/network/wireless/iwd.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/network/wireless/iwd.git/tree/COPYING) | **obfuscated** source code* |
| [kscreen](https://invent.kde.org/plasma/kscreen) | jupiter | [GPLv3](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [kmod](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git/tree/COPYING) | **obfuscated** source code* |
| [kupdate-notifier](https://gitlab.com/evlaV/kupdate-notifier) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kupdate-notifier/-/blob/master/LICENSE) | **obfuscated** source code* |
| [lib32-alsa-lib](https://github.com/alsa-project/alsa-lib) | jupiter | [LGPLv2.1](https://github.com/alsa-project/alsa-lib/blob/master/COPYING) | **obfuscated** source code* |
| [lib32-libnm](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [libevdi](https://github.com/DisplayLink/evdi) | jupiter | [GPLv2](https://github.com/DisplayLink/evdi/blob/devel/module/LICENSE), [LGPLv2.1](https://github.com/DisplayLink/evdi/blob/devel/library/LICENSE) | **obfuscated** source code* |
| [libxcrypt-compat](https://github.com/besser82/libxcrypt) | holo | [LGPLv2.1](https://github.com/besser82/libxcrypt/blob/develop/COPYING.LIB) | **obfuscated** source code* |
| [linux](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-firmware-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git) | jupiter | [GPLv3](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-3), [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-2) | **obfuscated** source code* |
| [linux-lts](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-61](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-60](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-rtw](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [makedumpfile](https://github.com/makedumpfile/makedumpfile) | holo | [GPLv2](https://github.com/makedumpfile/makedumpfile/blob/master/COPYING) | **obfuscated** source code* |
| [networkmanager](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [noise-suppression-for-voice](https://github.com/werman/noise-suppression-for-voice) | holo | [GPLv3](https://github.com/werman/noise-suppression-for-voice/blob/master/LICENSE) | **obfuscated** source code* |
| [pacman-system-update](https://github.com/gportay/pacman-system-update) | holo | [LGPLv2.1](https://github.com/gportay/pacman-system-update/blob/master/LICENSE) | **obfuscated** source code* |
| [paru](https://github.com/Morganamilo/paru) | holo | [GPLv3](https://github.com/Morganamilo/paru/blob/master/LICENSE) | **obfuscated** source code* |
| [pipewire](https://gitlab.freedesktop.org/pipewire/pipewire) | holo | [GPLv2](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE), [LGPL](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE) | **obfuscated** source code* |
| [plasma-nm](https://invent.kde.org/plasma/plasma-nm) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-3.0-only.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.1-only.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plasma-remotecontrollers](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers) | jupiter | [GPLv3](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-2.0-only.txt) | **obfuscated** source code* |
| [plasma-workspace](https://invent.kde.org/plasma/plasma-workspace) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.1-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plymouth](https://gitlab.freedesktop.org/plymouth/plymouth) | holo | [GPLv2](https://gitlab.freedesktop.org/plymouth/plymouth/-/blob/main/COPYING) | **obfuscated** source code* |
| [pyzy](https://github.com/pyzy/pyzy) | holo | [LGPLv2.1](https://github.com/pyzy/pyzy/blob/master/COPYING) | **obfuscated** source code* |
| [rauc](https://github.com/rauc/rauc) | holo | [LGPLv2.1](https://github.com/rauc/rauc/blob/master/COPYING) | **obfuscated** source code* |
| [rtl88x2ce-dkms](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/tree/master/rtl88x2ce-dkms) | jupiter | GPLv? | **obfuscated** source code* |
| [sddm-wayland](https://github.com/sddm/sddm) | jupiter | [GPLv2](https://github.com/sddm/sddm/blob/develop/LICENSE) | **obfuscated** source code* |
| [steamos-efi](https://gitlab.com/evlaV/steamos-efi) (fork of gnu-efi) | holo, jupiter | [GPLv2](https://gitlab.com/evlaV/steamos-efi/-/blob/master/COPYING) | **obfuscated** source code* |
| [udisks2](https://github.com/storaged-project/udisks) | jupiter | [GPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING), [LGPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING) | **obfuscated** source code* |
| [upower](https://gitlab.freedesktop.org/upower/upower) | jupiter | [GPLv2](https://gitlab.freedesktop.org/upower/upower/-/blob/master/COPYING) | **obfuscated** source code* |
| [vkmark](https://github.com/vkmark/vkmark) | holo | [LGPLv2.1](https://github.com/vkmark/vkmark/blob/master/COPYING-LGPL2.1) | **obfuscated** source code* |
| [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal) | jupiter | [LGPLv2.1](https://github.com/flatpak/xdg-desktop-portal/blob/main/COPYING) | **obfuscated** source code* |
| [xone-dkms](https://github.com/medusalix/xone) | holo | [GPLv2](https://github.com/medusalix/xone/blob/master/LICENSE) | **obfuscated** source code* |
| [xow](https://github.com/medusalix/xow) | holo | [GPLv2](https://github.com/medusalix/xow/blob/master/LICENSE) | **obfuscated** source code* |
| [zenity-light](https://gitlab.gnome.org/GNOME/zenity) | jupiter | [LGPLv2.1](https://gitlab.gnome.org/GNOME/zenity/-/blob/master/COPYING) | **obfuscated** source code* |

### First/Second Party (Valve, Collabora, and Igalia) Packages/Repositories in Violation of GNU License

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [kdump-steamos](https://gitlab.com/evlaV/kdump-steamos) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kdump-steamos/-/blob/main/README.md) | **obfuscated** source code* |
| [steamdeck-kde-presets](https://gitlab.com/evlaV/steamdeck-kde-presets) | jupiter | [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) | **obfuscated** source code* |
| [steamos-atomupd-client](https://gitlab.com/evlaV/steamos-atomupd) | holo | [LGPLv2](https://gitlab.com/evlaV/steamos-atomupd/-/blob/master/COPYING) | **obfuscated** source code* |
| [steamos-customizations](https://gitlab.com/evlaV/steamos-customizations) | holo | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-customizations-jupiter](https://gitlab.com/evlaV/steamos-customizations) | jupiter | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-media-creation](https://gitlab.com/evlaV/steamos-media-creation) | holo | [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) | **obfuscated** source code* |
| [steamos-netrc](https://gitlab.com/evlaV/holo-PKGBUILD/-/tree/master/steamos-netrc) | holo | GPLv? | **obfuscated** source code* |
| [steamos-repair-backend](https://gitlab.com/evlaV/steamos-repair-backend) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-backend/-/blob/main/COPYING) | **obfuscated** source code* |
| [steamos-repair-tool](https://gitlab.com/evlaV/steamos-repair-tool) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-tool/-/blob/master/COPYING) | **obfuscated** source code* |

***obfuscated** by one or both of the following:

1. ~~**no (written) offer for source code** - no official documentation or link(s) to source code (i.e., officially undocumented) - e.g., [Oracle](https://www.oracle.com/downloads/opensource/software-components-source-code.html) and [Sony PS5](https://doc.dl.playstation.net/doc/ps5-oss/index.html)~~ **(September 21, 2023)**
2. source code requires (officially undocumented) user alteration/modification/deobfuscation to be accessible/legible/usable/buildable (i.e., obfuscated) - see: [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
3. official (unmodified) PKGBUILD sources from / depends on [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/) (i.e., unbuildable); PKGBUILD provided here are modified to source from / depend on [these unofficial public mirrors](https://gitlab.com/evlaV) (i.e., buildable)

[**These public mirrors (@gitlab.com/evlaV)**](https://gitlab.com/users/evlaV/projects) aim to (unofficially) correct these violations.

***missing** source code:

- see: [GPL Package Research](#gpl-package-research)

---

**(September 21, 2023):** The following notice/offer was (finally) added [574 days after Steam Deck release](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&m2=9&d2=21&y2=2023&ti=on) to SteamOS (**Steam Version 1695334486**):

> **This was done purely in response to some back-and-forth between the [Software Freedom Conservancy](https://sfconservancy.org/about/) and [Valve's general/chief counsel (CLO) Liam Lavery](mailto:liaml@valvesoftware.com)**

<img alt="SteamOS Notice" src="images/steamos-notice-1.jpg" width="400">

<img alt="SteamOS Notice" src="images/steamos-notice-2.jpg" width="400">

> *Excuse me, I'm a bit of a stickler [Meeseeks], but **Valve, go home, you're drunk**; your operating system is [officially titled](https://repo.steampowered.com/steamos/README.txt) **SteamOS**, not **Steam OS***

---

### Manually Deobfuscate Source Package

| Path | Description |
| :--- | :--- |
| `$pkgname_dir_1`/ | Main package directory |
| \|__.SRCINFO | Arch Linux AUR package metadata |
| \|__PKGBUILD | Arch Linux package |
| \|__**`$pkgname_dir_2`/** | **Obfuscated package git** (directory name (`$pkgname_dir_2`) may be **identical to** or **differ from** (i.e., a derivative of) **main package directory** name (`$pkgname_dir_1`)) |

```sh
# (at your discretion) proceed to download and extract respective source package to current working directory ($PWD) (e.g., zenity-light)
curl https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz | bsdtar -xvf-

# respective package directory name(s) (e.g., zenity-light and zenity)
pkgname_dir_1=zenity-light
pkgname_dir_2=zenity

# deobfuscate
cd "$pkgname_dir_1/$pkgname_dir_2"
mkdir -p ".git/"
mv * ".git/"
git init
git checkout -f
```

---

### GPL Package Research

[Latest Steam Deck (jupiter) Recovery Image ("SteamOS Deck Image")](https://store.steampowered.com/steamos/download/?ver=steamdeck)

[Steam Deck (jupiter) Recovery Download Directory](https://steamdeck-images.steamos.cloud/recovery/)

---

**(February 28, 2022):** There were [542 GPL packages of 871 total packages (62.227%)](research/steam-deck-recovery-1-package-info.txt) distributed in [Steam Deck Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip))

> b2sum: `30453a4edab260d6a7af7c74b6287f6361e86d19e4d6d3b45a14c620d0bf2540ac3f281b5947e9541844db41c79681b18156e7db96a9ce95a23e52ba6409e2c9 steamdeck-recovery-1.img.bz2`

---

**(May 15, 2022):** There were [543 GPL packages of 871 total packages (62.342%)](research/steam-deck-recovery-4-package-info.txt) distributed in [Steam Deck Recovery Image 4](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.zip))

> b2sum: `ab806fb2374db4135b1a706425c81d367aeeb8508b4417ba652438e52772b88462f029a257f193d6827df8e526bbad9b10c532ef1a437b7393312e5fd4ad9c0e steamdeck-recovery-4.img.bz2`

---

**(as of June 17, 2023):** There were [593 GPL packages of 944 total packages (62.817%)](research/steam-deck-jupiter-main-package-info-20230617.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

**(as of September 21, 2023):** There were [595 GPL packages of 971 total packages (61.277%)](research/steam-deck-jupiter-main-package-info.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

---

*Thank you [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)*; *I know how truly **difficult** and **lengthy/time-consuming** GitLab repository publication can be*; [the following how-to](#-gitlab-repository-private-public-how-to-) was made specifically for you:

## 😉 GitLab Repository Private-Public How-to 😂

The following must be conducted per [GitLab repository](https://gitlab.steamos.cloud/):

1. Left click `Settings` or mouseover `Settings` then left click `General` (`Settings > General`)
2. Expand `Visibility, project features, permissions` (left click `Expand`)
3. Change `Project visibility` dropdown menu option from `Private` to `Public` (left click `Private` then left click `Public`)
4. Left click `Save changes`

Here's a visual example using `jupiter-hw-support`:

<img alt="GitLab Private-Public How-to" src="images/gitlab-private-public-how-to.gif" width="512">

Maybe [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) will perform this *daunting* task in ~~2022~~ 2023 (?) 🤞

## 💭 Thoughts/Opinions 💡

Valve has commercialized a product (SteamOS 3.x / Steam Deck) which almost exclusively relies on and utilizes (free/libre) open source software. Software myself and countless others have contributed to for decades. Valve has capitalized/profited immensely from (free/libre) open source software, and in turn has not been very friendly to the (free/libre) open source software community. This is why (in protest) I set up these public mirrors on April 1, 2022. **I chose April Fools' Day as a nod to the foolishness of Valve regarding publication.**

Valve likes to distance themselves from proprietary technology companies (e.g., Microsoft Windows) but Valve SteamOS isn't much different; SteamOS is a privately developed OS, with proprietary software and DRM (Steam) front and center, consisting of mostly public sources ([the majority of which are GPL](#gpl-package-research)). The Steam Deck additionally contains proprietary hardware, which includes additional proprietary software (this is also true for the [Steam Deck Docking Station](https://www.steamdeck.com/en/dock)). This can thankfully be (mostly) alleviated/liberated by [OpenSD](https://gitlab.com/open-sd/opensd) (thank you [@seek-dev](https://gitlab.com/seek-dev)).

Paired with Valve's deliberate privatization of their GitLab repositories and poor interactions regarding publication (over **18 months** later) makes it comparatively easy to recommend the [ASUS ROG Ally](https://rog.asus.com/gaming-handhelds/rog-ally/rog-ally-2023/) (June 13, 2023). Who doesn't love a global launch? Moreover, the similar nature of [publication date](https://www.theverge.com/2023/4/1/23666084/asus-rog-ally-handheld-windows-gaming-portable) (April Fools' Day). And like the Steam Deck, you're free to install **YOUR CHOICE of OS** (or use pre-installed Windows 11). Microsoft Windows and Valve SteamOS are pretty comparable to me, though I'd argue Windows 11 behaves as expected outside of gaming (desktop use). SteamOS 3.x / Steam Deck does not behave or perform anything like Arch / GNU/Linux. It's tragic knowing some people's first/early interactions with GNU/Linux will be via SteamOS. What a terrible/inaccurate representation of GNU/Linux (to say the least). Regardless, I'd recommend the same for both (Steam Deck and ROG Ally): take advantage of the hardware and install something (anything) else.

I'd advocate the openness of Valve and the Steam Deck, but I unfortunately cannot attest to that. SteamOS 3.x is a terrible (and I honestly think worthy of the title "worst") distribution of Linux (e.g., SteamOS 3.x doesn't comply with [Arch Linux](https://archlinux.org/about/) or [GNU/Linux](https://www.gnu.org/philosophy/philosophy.html) philosophies), with Steam (proprietary and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management)) front and center, all else is secondary (or nonexistent); this unfortunately includes the (free/libre) open source software community, who are the very foundation which SteamOS is built upon. Look at the polarity between SteamOS 3.x / Steam Deck Gaming mode versus Desktop (and Recovery) mode to see where Valve's focus lies. The majority of Steam Deck updates enhance Steam / Gaming mode. 'Switch to Desktop' is the bottom/last Power option (excluding 'Cancel'). 'Return to Gaming Mode' (the mode the system (always) boots to) is the first (primary) Desktop option. SteamOS 3.x / Steam Deck is buttery smooth and pleasant in Steam / Gaming mode, compared to rough/painful (to say the least) in Desktop mode.

To say a bit more, SteamOS is rather easy to [soft brick](https://en.wikipedia.org/wiki/Brick_(electronics)#Soft_brick) (I've soft bricked SteamOS more times than I'd like to admit). SteamOS (jupiter) lacks a C/C++ compiler (e.g., GCC / glibc or Clang), doesn't have a very expansive/functional instance of Python (e.g., no Python package / pip support), can't traditionally install packages/software or retain modifications, and has a wonky bootloader (e.g., dual booting). Dual booting can beautifully be resolved by [SteamDeck-Clover-Dualboot](https://github.com/ryanrudolfoba/SteamDeck-Clover-Dualboot) (thank you [@ryanrudolfoba](https://github.com/ryanrudolfoba)).

It's apparent Valve doesn't expect (or prioritize) the use of Desktop mode. A huge and completely missed opportunity for Valve, SteamOS, Steam Deck, GNU/Linux, and the entire community. I think GNOME would be a much better desktop environment, especially for touch screen devices like the Steam Deck (or any portable for that matter), but I believe Valve chose KDE due to their [special (financial) interests](https://www.phoronix.com/news/Valve-Funding-KWin-Work). So the Steam Deck and the community suffer only to "justify" Valve's investments in KDE development. Money talks. Why not embrace the many nuances of Arch / GNU/Linux (and its users) and enable the use of any (Arch supported) desktop environment?

Many of Valve's commercial/hardware projects/products fail (e.g., SteamOS 1 and 2, Steam Link, and Steam Controller); the truly sad thing is Valve's ideas are generally good (on paper), but (as Valve has repeatedly demonstrated) are extremely poorly managed and executed by uncoordinated, unmotivated, ignorant, incompetent, and **unethical** people. I believe this is a side effect of Valve's ['flat hierarchy' facade](https://www.theguardian.com/commentisfree/2018/jul/30/no-bosses-managers-flat-hierachy-workplace-tech-hollywood). I predict SteamOS 3.x will have many of the same issues (and more) SteamOS 1 and 2 had. This is further compounded by Valve following/projecting a similar canonical/conventional (milestone) ideology (from Debian) to Arch. **This concept/implementation is deeply flawed, incompatible, and conflicts with Arch's bleeding edge rolling release! Asinine!**

**SteamOS is technically Linux; oriented to directly benefit Valve (primarily/solely promoting itself (Steam) and DRM); Valve is very much like any other corporation who markets proprietary/non-free/closed-source software/OS (e.g., Apple macOS and Microsoft Windows). SteamOS possesses none of the [philosophy of GNU](https://www.gnu.org/philosophy/philosophy.html). And deserves a proper (up-to-date) [GNU unendorsement](https://www.gnu.org/distros/common-distros.html).**

Valve clearly aren't interested in public access to their code or contributions. I'm honestly questioning if Valve will ever publicize their GitLab repositories or open SteamOS development to the public (contributions and issues). I'll likely be maintaining these public mirrors for the foreseeable future...

This is not ideal, as I (an unaffiliated third party) should not be trusted to host Valve's entire SteamOS 3.x / Steam Deck [codebase](https://en.wikipedia.org/wiki/Codebase). I haven't made any alterations, except (when necessary) pointing to [these unofficial public mirrors](https://gitlab.com/evlaV) instead of [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/). Users of these unofficial public mirrors have to trust me, and there isn't an easy or intuitive way to verify the code on these public mirrors hasn't been tampered with (because Valve's official GitLab repositories are still private). With skepticism, I hope these public mirrors serve as the de facto place to source, verify, and follow official SteamOS 3.x / Steam Deck software/updates.

I've graciously done Valve's ("last mile") source code distribution job/work duty (free of charge) for over **18 months** now, and will continue to do so for the foreseeable future, hopefully filling the void left from Valve. Through this void filling effort, I've interacted with several developers of projects who have benefited from these public mirrors. From individual/add-on projects like [SteamOS Btrfs](https://gitlab.com/popsulfr/steamos-btrfs) (thank you [@popsulfr (Philipp Richter)](https://gitlab.com/popsulfr)) to entire replacement Linux distributions like [batocera](https://batocera.org/), [bazzite](https://github.com/ublue-os/bazzite), and [ChimeraOS](https://chimeraos.org/). That's precisely the purpose of this project; to aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers. If you have any questions or comments, [write to me](mailto:valve.evlav@proton.me).

---

*"The truly intelligent person is one who can pretend to be a fool in front of a fool who pretends to be intelligent."* ―

---

*"The best life is the one in which the creative impulses play the largest part and the possessive impulses the smallest."* ― **Bertrand Russell, Pacifism and Revolution**

---

*"I should like to say two things, one intellectual and one moral:*

*The intellectual thing I should want to say to them is this: When you are studying any matter or considering any philosophy, ask yourself only what are the facts and what is the truth that the facts bear out. Never let yourself be diverted either by what you wish to believe or by what you think would have beneficent social effects if it were believed, but look only and solely at what are the facts. That is the intellectual thing that I should wish to say.*

*The moral thing I should wish to say to them is very simple. I should say: **Love is wise, hatred is foolish. In this world, which is getting more and more closely interconnected, we have to learn to tolerate each other. We have to learn to put up with the fact that some people say things that we don’t like. We can only live together in that way, and if we are to live together and not die together we must learn a kind of charity and a kind of tolerance which is absolutely vital to the continuation of human life on this planet.**"* ― **Bertrand Russell, [BBC Face to Face (1959)](https://www.youtube.com/watch?v=a10A5PneXlo&t=1646s)**

---

*"Education can help us only if it produces “whole men”. The truly educated man is not a man who knows a bit of everything, not even the man who knows all the details of all subjects (if such a thing were possible): the “whole man” in fact, may have little detailed knowledge of facts and theories, he may treasure the Encyclopædia Britannica because “she knows and he needn’t”, but he will be truly in touch with the centre. He will not be in doubt about his basic convictions, about his view on the meaning and purpose of his life. He may not be able to explain these matters in words, but the conduct of his life will show a certain sureness of touch which stems from this inner clarity."* ― **E. F. Schumacher, Small is Beautiful: A Study of Economics as if People Mattered**

---

*"People who talk well but do nothing are like musical instruments; the sound is all they have to offer."* ― **Diogenes, Herakleitos and Diogenes**

---

*"Missing from such histories are the countless small actions of unknown people that led up to those great moments. When we understand this, we can see that the tiniest acts of protest in which we engage may become the invisible roots of social change."* ― **Howard Zinn, You Can't Be Neutral on a Moving Train: A Personal History of Our Times**

---

*"The best people possess a feeling for beauty, the courage to take risks, the discipline to tell the truth, the capacity for sacrifice. Ironically, their virtues make them vulnerable; they are often wounded, sometimes destroyed."* ― **Ernest Hemingway, The Letters of Ernest Hemingway**

---

*"Keep away from people who try to belittle your ambitions. Small people always do that, but the really great make you feel that you, too, can become great."* ― **Mark Twain**

---

*“Without music, life would be a mistake.”* ― **Friedrich Nietzsche, Twilight of the Idols**

---

🎵 *I'm the voice inside your head, you refuse to hear. I'm the face that you have to face, mirroring your stare. I'm what's left, I'm what's right, I'm the enemy. I'm the hand that'll take you down, bring you to your knees. So, who are you?* 🎵

🎵 ***Keep you in the dark, you know they all pretend.*** 🎵

---

*"If you scratch a cynic, you'll find a disappointed idealist."* ― **George Carlin**

---

*"The optimist proclaims that we live in the best of all possible worlds; and the pessimist fears this is true."* ― **James Branch Cabell, The Silver Stallion**

*"The wickedness and the foolishness of no man can avail against the foolishness and the fond optimism of man-kind."* ― **James Branch Cabell, The Silver Stallion**

---

*"A pessimist is a well-informed optimist."* ― **Mark Twain**

---

*"Whoever fights with monsters should see to it that he does not become one himself. And when you stare for a long time into an abyss, the abyss stares back into you."* ― **Friedrich Nietzsche, Beyond Good and Evil**

---

**(May 28, 2023):** The following [free speech flags](https://en.wikipedia.org/wiki/Free_Speech_Flag) belong here because [reasons](https://dolphin-emu.org/blog/6T/) ([psychological reactance](https://en.wikipedia.org/wiki/Reactance_(psychology)) / [Streisand effect](https://en.wikipedia.org/wiki/Streisand_effect)) | [update](https://www.theverge.com/2023/6/1/23745772/valve-nintendo-dolphin-emulator-steam-emails) | [aftermath](https://dolphin-emu.org/blog/6U/):

<img alt="Free Speech Flag (Wii)" src="images/free-speech-flag-wii.png" width="480">

<img alt="Free Speech Flag (Wii U)" src="images/free-speech-flag-wii-u.png" width="480">

- Wii Common Key: **EBE42A225E8593E448D9C5457381AAF7** | [bin](keys/wii-common-key.bin) | [txt](keys/wii-common-key.txt)
- Wii U Common Key: **D7B00402659BA2ABD2CB0DB27FA2B656** | [bin](keys/wii-u-common-key.bin) | [txt](keys/wii-u-common-key.txt)

## 📜 License 📜

The contents of [this project (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) are licensed under the [Mozilla Public License Version 2.0 (MPL 2.0)](LICENSE), and the underlying packages are licensed under their respective license(s) (see: respective package repository, directory, and/or PKGBUILD).

The following contents are [free, unencumbered, and released into the public domain](UNLICENSE) or (equivalently) licensed under the [BSD Zero Clause License (0-clause)](LICENSE.0BSD):

- [`images/free-speech-flag-wii.svg`](images/free-speech-flag-wii.svg) ([`images/free-speech-flag-wii.png`](images/free-speech-flag-wii.png))
- [`images/free-speech-flag-wii-u.svg`](images/free-speech-flag-wii-u.svg) ([`images/free-speech-flag-wii-u.png`](images/free-speech-flag-wii-u.png))
